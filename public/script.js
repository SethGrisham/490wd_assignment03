document.addEventListener("DOMContentLoaded", function() {
    const username = document.getElementById('username');
    const usernameError = document.getElementById('usernameError');
    const usernameValid = document.getElementById('usernameValid');
    const usernamePattern = /^[a-zA-Z_]{6,10}$/;
    
    const email = document.getElementById('email');
    const emailError = document.getElementById('emailError');
    const emailValid = document.getElementById('emailValid');
    const emailPattern = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
    
    const position = document.getElementById('position');
    const positionError = document.getElementById('positionError');
    const positionValid = document.getElementById('positionValid');
    
    const fulltime = document.getElementById('fulltime');
    const parttime = document.getElementById('parttime');
    const typeError = document.getElementById('typeError');
    const typeValid = document.getElementById('typeValid');

    const form = document.getElementById('form');
    
    form.addEventListener('submit', function(e) {
	let valid = true;

	if (!usernamePattern.test(username.value)) {
	    usernameError.textContent = "Username must be 6-10 characters with letters or an underscore";
	    usernameValid.textContent = "";
	    username.style.border = "1px solid red";
	    usernameError.style.color = "red";
	    valid = false;
	} else {
	    usernameError.textContent = "";
	    username.style.border = "";
	    usernameValid.textContent = "✅";
	}

	if (!emailPattern.test(email.value)) {
	    emailError.textContent = "Not a valid e-mail address";
	    emailValid.textContent = "";
	    email.style.border = "1px solid red";
	    emailError.style.color = "red";
	    valid = false;
	} else {
	    emailError.textContent = "";
	    email.style.border = "";
	    emailValid.textContent = "✅";
	}

	if (position.value === "none") {
	    positionError.textContent = "You must select a position";
	    positionValid.textContent = "";
	    position.style.border = "1px solid red";
	    positionError.style.color = "red";
	    valid = false;
	} else {
	    positionError.textContent = "";
	    position.style.border = "";
	    positionValid.textContent = "✅";
	}

	if (!fulltime.checked && !parttime.checked) {
	    typeError.textContent = "You must select a type";
	    typeValid.textContent = "";
	    typeError.style.color = "red";
	    valid = false;
	} else {
	    typeError.textContent = "";
	    typeValid.textContent = "✅";
	}

	if (!valid) {
	    e.preventDefault()
	}
    });
});
